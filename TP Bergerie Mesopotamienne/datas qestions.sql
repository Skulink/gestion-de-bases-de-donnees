USE bergerieroyale;

-- nb de moutons encore présent dans la bergerie classé par sexe
SELECT COUNT(id), sexe FROM mouton WHERE valide=1 GROUP BY sexe;

-- connaître la confiance accordée aux travailleurs selon leur origine
SELECT travailleur.nom AS travailleur, note_confiance AS note, ville.nom AS origine FROM travailleur 
JOIN ville ON ville.id=travailleur.id_ville_origine 
WHERE id_ville_origine IS NOT null ORDER BY note DESC;

-- connaître la récurrence des thèmes des présages
SELECT COUNT(id_presage) AS recurrence, categorie.nom FROM lien_categorie_presage 
JOIN categorie ON id_categorie=categorie.id 
GROUP BY categorie.nom ORDER BY recurrence DESC;

-- lister les concours qui pourraient être affectés par les offrandes faites par le palais de Mari 
-- dans les temples appartenant à la ville organisatrice
SELECT concours.nom, lieu.nom, offrande.annee_offrande FROM offrande
JOIN lieu ON id_temple=lieu.id 
JOIN lien_concours_ville ON lien_concours_ville.id_ville = lieu.id_ville
JOIN concours ON concours.id=lien_concours_ville.id_concours 
WHERE role="organisatrice";

-- connaître le nombre de moutons donnés en offrande les années où Mari a gagné aux concours
SELECT SUM(quantite) AS nombre_mouton_offert, annee_offrande FROM offrande 
WHERE annee_offrande IN(SELECT annee_concours FROM concours
JOIN lien_concours_ville ON lien_concours_ville.id_concours=concours.id 
JOIN ville ON lien_concours_ville.id_ville = ville.id 
WHERE ville.nom="Mari" AND role="gagnante");

-- classement des 10 meilleurs moutons de Mari, basé sur leur classement aux concours
SELECT mouton.nom, classement_mouton FROM mouton 
JOIN participant ON mouton.id=participant.id_mouton 
WHERE classement_mouton <=(SELECT AVG(classement_mouton) FROM participant) ORDER BY classement_mouton LIMIT 0,10;

-- donne le nombre de moutons perdus sous le règne de Zimrî-Lîm et listé selon le berger (il y a plusieurs bergers par bergerie)
SELECT count(distinct(mouton)) AS nb_mouton_perdu, berger FROM perte_par_berger 
WHERE date_mort LIKE "ZL%" 
GROUP BY berger ORDER BY nb_mouton_perdu DESC;

-- donne les moutons enfants des moutons au meilleur classement
SELECT mouton.nom AS meilleur_espoir FROM mouton 
JOIN parente ON parente.id_mouton_e=mouton.id 
WHERE id_mouton_p IN(SELECT id_mouton FROM participant WHERE classement_mouton <=10);

-- selectionner au hasard, 3 agneaux mâles (qui pourront être destinés à la divination)
SELECT id FROM mouton WHERE valide=1 AND sexe="mâle" AND anneedn="ZL10" ORDER BY rand() LIMIT 0,3;

-- donne la quantité de moutons maximale offerte lors des offrandes et la destination de ces offrandes
SELECT MAX(quantite), lieu.type, lieu.nom FROM offrande 
JOIN lieu ON offrande.id_temple=lieu.id;

-- donne la cuisine qui a reç la plus grosse offrande
SELECT lieu.type, lieu.nom, quantite FROM lieu 
JOIN table_royale ON id_cuisine=lieu.id 
HAVING max(quantite);