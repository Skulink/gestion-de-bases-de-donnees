/* requete numero 1 : quelle bataille à été la plus sanglante */

SELECT bataille.lieu, count(soldat.id_cite) as nb_soldat
FROM bataille
INNER JOIN bataillon ON bataille.id_bataille = bataillon.id_bataille
INNER JOIN lien_soldat_bataillon ON bataillon.id_bataillon = lien_soldat_bataillon.id_bataillon
INNER JOIN soldat ON lien_soldat_bataillon.id_soldat = soldat.id
GROUP BY bataille.lieu
ORDER BY nb_soldat DESC;
LIMIT 1;

/* requete numero 2 : classer les cites par nombre de soldat */

SELECT cite.nom, count(soldat.id_cite) as nb_soldat
FROM cite
INNER JOIN soldat ON cite.id = soldat.id_cite
GROUP BY cite.nom
ORDER BY nb_soldat DESC

/* requete numero 3 : quel statut a le moins de chance de mourir */

SELECT statut.nom, count(soldat.date_mort != "") AS soldats_morts
FROM statut
INNER JOIN lien_corps_statut ON statut.id = lien_corps_statut.statut_id
INNER JOIN corps ON lien_corps_statut.corps_id = corps.id
INNER JOIN soldat ON corps.id = soldat.corps_id
GROUP BY statut.nom
ORDER BY soldats_morts ASC
LIMIT 1;

/* requete numero 4 : afficher le stock d'armes et d'armures par ville */

SELECT q1.id_cite, armes , armures 
FROM (SELECT id_cite, SUM(nb_armes) as armes 
FROM stock_armes 
GROUP BY id_cite) q1 
LEFT JOIN (SELECT id_cite, SUM(nb_armures) as armures 
FROM stock_armures 
GROUP BY id_cite) q2 ON (q1.id_cite = q2.id_cite)

/* requete numero 5: Dans quelle campagne y a t-il eu le moins de mort */

SELECT campagne.nom, count(soldat.date_mort != "") AS soldats_morts
FROM campagne
INNER JOIN bataille ON campagne.id =  bataille.id_campagne
INNER JOIN bataillon ON bataille.id_bataille = bataillon.id_bataille 
INNER JOIN lien_soldat_bataillon ON bataillon.id_bataillon = lien_soldat_bataillon.id_bataillon
INNER JOIN soldat ON lien_soldat_bataillon.id_soldat =  soldat.id
GROUP BY campagne.nom
ORDER BY soldats_morts ASC;

/* requete numero 6 : quelle est la durée de vie moyenne d'un marin */

SELECT AVG(((date_naissance - date_mort)*-1)) 
FROM soldat 
WHERE (corps_id = 3 
AND date_mort != "");

/* requete numero 7 : combien y-a -t'il de soldat dans la marine */

SELECT corps.nom, count(soldat.date_mort <> "") as soldat_vivants
FROM corps
INNER JOIN soldat ON corps.id = soldat.corps_id



/* requete numero 8 : combien de soldats ont des flûtes */

SELECT count(armes.nom)
FROM armes
INNER JOIN lien_armes_statut ON armes.id = lien_armes_statut.armes_id
INNER JOIN statut ON lien_armes_statut.statut_id = statut.id
INNER JOIN lien_corps_statut ON statut.id = lien_corps_statut.statut_id
INNER JOIN corps ON lien_corps_statut.corps_id = corps.id
INNER JOIN soldat ON corps.id = soldat.corps_id
WHERE armes.nom = "Lance"


/* requete numéro 9 : combien de soldats n'on pas de cuirasse mais on des sandales */

