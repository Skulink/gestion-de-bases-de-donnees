-- phpMyAdmin SQL Dump
-- version 4.6.0
-- http://www.phpmyadmin.net
--
-- Client :  localhost
-- Généré le :  Mar 26 Avril 2016 à 19:03
-- Version du serveur :  5.5.47-0+deb8u1
-- Version de PHP :  5.6.17-0+deb8u1
USE bergerieroyale;

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `bergerieroyale`
--

--
-- Contenu de la table `annee` via fichier annee.csv
--

LOAD DATA LOCAL INFILE '../noemie.csv' 
REPLACE INTO TABLE annee
FIELDS TERMINATED BY ','
ENCLOSED BY '"'
LINES STARTING BY ''
TERMINATED BY '\n'
(code_annee, nom);

--
-- Contenu de la table `categorie`
--

INSERT INTO `categorie` (`id`, `nom`) VALUES
(1, 'maladie'),
(2, 'mort'),
(3, 'militaire'),
(4, 'justice'),
(5, 'famille royale');

--
-- Contenu de la table `espece`
--

INSERT INTO `espece` (`id`, `nom`) VALUES
(1, 'de Sumer'),
(2, 'amorrite'),
(3, 'de Magan'),
(4, 'à grosse queue');

--
-- Contenu de la table `profession`
--

INSERT INTO `profession` (`id`, `nom`, `responsable`) VALUES
(1, 'devin', 0),
(2, 'berger', 1),
(3, 'berger', 0),
(4, 'boucher', 1),
(5, 'boucher', 0),
(6, 'artisan du cuir', 1),
(7, 'artisan du cuir', 0);

--
-- Contenu de la table `ville`
--

INSERT INTO `ville` (`id`, `nom`, `royaume`) VALUES
(1, 'Mari', 'Mari'),
(2, 'Terqa', 'Mari'),
(3, 'Saggaratum', 'Mari'),
(4, 'Qattunan', 'Mari'),
(5, 'Dur-Yahdun-Lim', 'Mari'),
(6, 'Samanum', 'Mari'),
(7, 'Babylone', 'Babylone'),
(8, 'Dûr-Kurigalzu', 'Babylone'),
(9, 'Sippar de Samas', 'Babylone'),
(10, 'Sippar d\'Annunîtum', 'Babylone'),
(11, 'Borsippa', 'Babylone'),
(12, 'Nippur', 'Babylone'),
(13, 'Uruk', 'Babylone'),
(14, 'Eshnunna', 'Eshnunna'),
(15, 'Shaduppum', 'Eshnunna'),
(16, 'Arrapha', 'Eshnunna'),
(17, 'Nerebtum', 'Eshnunna'),
(18, 'Alep', 'Alep'),
(19, 'Alalah', 'Alep'),
(20, 'Ugarit', 'Alep');

--
-- Contenu de la table `lieu`
--

INSERT INTO `lieu` (`id`, `type`, `nom`, `id_ville`) VALUES
(1, 'temple', 'd\'Ishtar', 1),
(2, 'temple', 'de Ninhursag', 1),
(3, 'temple', 'de Samas', 1),
(4, 'temple', 'de Ninni-Zaza', 1),
(5, 'temple', 'de Dagan', 1),
(6, 'temple', 'de Dagan', 2),
(7, 'temple', 'de Ninkarrak', 2),
(8, 'temple', 'de Dagan', 3),
(9, 'temple', 'd\'Annunitum', 6),
(10, 'cuisine', 'du bas', 1),
(11, 'cuisine', 'de la reine', 1),
(12, 'cuisine', 'des épouses', 1),
(13, 'cuisine', 'de l\'est', 1),
(14, 'cuisine', 'du palmier', 1),
(15, 'atelier', 'du gouverneur', 2),
(16, 'atelier', 'du palais', 1),
(17, 'atelier', 'de l\'ambassade', 5),
(18, 'atelier', 'du prince', 1),
(19, 'bergerie', 'de la vallée est', 1),
(20, 'bergerie', 'de la vallée ouest', 1),
(21, 'bergerie', 'du plateau', 1),
(22, 'bergerie', 'du gouverneur', 2),
(23, 'bergerie', 'du palais', 1),
(24, 'bergerie', 'des Benjaminites', 3),
(25, 'bergerie', 'du canal', 1),
(26, 'bergerie', 'du nouveau canal', 2);

--
-- Contenu de la table `cuir`
--

INSERT INTO `cuir` (`id`, `id_atelier`, `date_depot`, `annee_depot`) VALUES
(1, 16, '02/08', 'ZL01'),
(2, 16, '12/08', 'ZL01'),
(3, 17, '05/11', 'ZL07'),
(4, 15, '12/09', 'ZL10'),
(5, 18, '23/05', 'SY03'),
(6, 16, '19/10', 'ZL09'),
(7, 17, '12/09', 'ZL10');

--
-- Contenu de la table `offrande`
--

INSERT INTO `offrande` (`id`, `id_temple`, `date_offrande`, `annee_offrande`, `quantite`, `valide`) VALUES
(1, 9, '01/04', 'ZL08', 5, 1),
(2, 7, '12/12', 'ZL09', 2, 1),
(3, 3, '21/05', 'ZL09', 3, 1),
(4, 8, '04/08', 'ZL10', 1, 1),
(5, 4, '25/01', 'ZL08', 2, 1);

--
-- Contenu de la table `table_royale`
--

INSERT INTO `table_royale` (`id`, `id_cuisine`, `date_mise_en_reserve`, `annee_mise_en_reserve`, `quantite`, `valide`) VALUES
(1, 14, '11/09', 'ZL10', 3, 1),
(2, 11, '06/09', 'ZL10', 2, 1),
(3, 12, '01/09', 'ZL10', 2, 1),
(4, 10, '03/09', 'ZL10', 4, 1),
(5, 11, '05/09', 'ZL10', 1, 1),
(6, 13, '06/09', 'ZL10', 2, 1),
(7, 14, '09/09', 'ZL10', 5, 1),
(8, 14, '17/10', 'ZL09', 1, 1);

--
-- Contenu de la table `travailleur`
--

INSERT INTO `travailleur` (`id`, `nom`, `id_profession`, `id_lieu_travail`, `id_ville_origine`, `date_debut_partenariat`, `annee_debut_partenariat`, `note_confiance`, `date_fin_partenariat`, `annee_fin_partenariat`, `valide`) VALUES
(1, 'Ahi-Maras', 1, 9, NULL, NULL, 'ZL01', 5, NULL, NULL, 1),
(2, 'Ilab-tarik', 1, 8, NULL, '02/11', 'ZL05', NULL, NULL, NULL, 1),
(3, 'Naram-Sîn', 1, 3, 14, NULL, 'YL16', 5, NULL, NULL, 1),
(4, 'Ibbi-Samas', 1, 7, 10, '07/10', 'ZL09', 0, '09/04', 'ZL10', 0),
(5, 'Asqudum', 1, 2, 18, NULL, 'YL13', 2, NULL, NULL, 1),
(6, 'Sîn-nada', 2, 19, 7, '08/10', 'ZL07', 4, NULL, NULL, 1),
(7, 'Warad-Sîn', 3, 19, NULL, NULL, 'SY02', 5, NULL, NULL, 1),
(8, 'Ipku-ersetum', 2, 19, NULL, NULL, 'YL17', 3, '12/06', 'ZL02', 0),
(9, 'Inuh-Esagil', 3, 19, 19, '09/07', 'ZL01', 5, NULL, NULL, 1),
(10, 'Sha-pi-Êl', 2, 20, 17, NULL, NULL, 4, '06/02', 'ZL08', 0),
(11, 'Saniq-pi-Samas', 2, 20, NULL, '10/02', 'ZL08', 4, NULL, NULL, 1),
(12, 'Ilshu-ibni', 3, 20, 8, NULL, NULL, 5, NULL, NULL, 1),
(13, 'Yaggih-Addu', 2, 21, NULL, '24/03', 'ZL10', 2, NULL, NULL, 1),
(14, 'Sîn-tiri', 3, 21, NULL, NULL, 'SY02', 5, NULL, NULL, 1),
(15, 'Ibal-pi-Marduk', 3, 21, 7, NULL, 'ZL04', NULL, NULL, NULL, 1),
(16, 'Asqur-Lîm', 2, 22, NULL, NULL, 'ZL02', 5, NULL, NULL, 1),
(17, 'Zazya', 3, 22, 12, NULL, 'YL15', 4, NULL, NULL, 1),
(18, 'Addu-Sharrum', 4, 10, NULL, '25/10', 'ZL09', NULL, NULL, NULL, 1),
(19, 'Hamatil', 5, 10, NULL, NULL, 'YL14', 5, NULL, NULL, 1),
(20, 'Sharrum-ma-El', 4, 11, 9, NULL, 'ZL03', 3, NULL, NULL, 1),
(21, 'Sîn-tappê', 5, 11, NULL, NULL, NULL, 5, '12/12', 'ZL09', 0),
(22, 'Ishar-Lîm', 5, 11, NULL, '30/11', 'ZL04', 4, NULL, NULL, 1),
(23, 'Idin-Annu', 4, 12, 11, NULL, 'ZL08', 2, NULL, NULL, 1),
(24, 'Ilu-na-qêri', 5, 12, NULL, NULL, NULL, 5, NULL, NULL, 1),
(25, 'Tâb-el-ummâni', 4, 13, NULL, NULL, 'SY03', 4, NULL, NULL, 1),
(26, 'Idin-Mamma', 6, 15, 14, NULL, 'ZL10', 5, NULL, NULL, 1),
(27, 'Hayya-Addu', 7, 15, 13, NULL, 'YL16', 4, NULL, NULL, 1),
(28, 'Yar\'ip-El', 6, 16, NULL, NULL, 'YL13', 3, NULL, NULL, 1),
(29, 'Dârish-Lîbûr', 7, 16, NULL, NULL, 'ZL03', 5, '16/07', 'ZL07', 0),
(30, 'Balu-Eshtar', 6, 17, 20, '14/03', 'ZL05', 5, NULL, NULL, 1),
(31, 'Ayya-Meshlu', 2, 24, NULL, NULL, 'ZL00', 5, NULL, NULL, 1),
(32, 'Mut-Bisir', 2, 23, 15, '24/09', 'ZL03', 4, NULL, NULL, 1);

--
-- Contenu de la table `presage`
--

INSERT INTO `presage` (`id`, `date_presage`, `annee_presage`, `id_devin`, `question`, `description`, `valide`) VALUES
(1, '07/04', 'ZL10', 4, 'Les deux rebelles de l\'armée bédouine de Suhum, Darish-Libur et hali-Hadun, doivent-il être condamné à mort ?', 'Présence de 2 tallums. Les présages sont bons. Les rebelles peuvent être exécutés sans risque de soulèvement dans l\'armée.', 1),
(2, '13/09', 'ZL06', 5, 'Beltani va-t-elle survivre à sa maladie ?', 'Les présages sont favorables.\r\n', 1),
(3, '06/10', 'ZL07', 2, 'La route de général Yatir-Nânnum vers Eshnunna est-elle sûre ?', 'Le martum est à sa place ; la voie gauche de l\'amère est double.\r\nLes dieux nous préviennent du danger. Présage défavorable.', 1);

--
-- Contenu de la table `lien_categorie_presage`
--

INSERT INTO `lien_categorie_presage` (`id_presage`, `id_categorie`) VALUES
(2, 5),
(2, 1),
(1, 3),
(1, 2),
(3, 3);

--
-- Contenu de la table `concours`
--

INSERT INTO `concours` (`id`, `nom`, `date_concours`, `annee_concours`, `id_ambassadeur`, `description`, `valide`) VALUES
(1, 'Concours de brebis amorrite', '28/11', 'ZL09', 6, 'La bergerie royale de Mari a remporté le 1er prix grâce à sa merveilleuse brebis, Sîn-Marduk. L\'ambassadeur d\'Elamite a proféré des menaces à l\'encontre du roi Zimrî-Lîm.', 1),
(2, 'Concours de la brebis tachetée', '03/01', 'ZL08', 10, 'Prix remporté par Alep et la brebis royale. On dit qu\'elle mange à la table du roi.\r\n', 1),
(3, 'Concours du mâle reproducteur', '23/03', 'ZL08', 14, 'Victoire de Mari. Babylone propose l\'achat du mouton gagnant mais l\'ambassadeur de Mari décline l\'offre', 1);

--
-- Contenu de la table `lien_concours_ville`
--

INSERT INTO `lien_concours_ville` (`id_concours`, `id_ville`, `role`) VALUES
(1, 7, 'organisatrice'),
(1, 1, 'gagnante'),
(2, 18, 'gagnante'),
(2, 4, 'organisatrice'),
(3, 1, 'organisatrice'),
(3, 1, 'gagnante');

--
-- Contenu de la table `mouton`
--

INSERT INTO `mouton` (`id`, `nom`, `sexe`, `id_espece`, `couleur`, `id_bergerie`, `ddn`, `anneedn`, `date_sortie`, `annee_sortie`, `id_cuir`, `id_table_royale`, `id_offrande`, `id_presage`, `mort_prematuree`, `valide`) VALUES
(1, 'Daddu-Sîn', 'femelle', 2, 'blanc', 19, '03/04', 'ZL08', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1),
(2, 'Sîn-Marduk', 'femelle', 2, 'blanc', 19, '12/05', 'ZL07', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1),
(3, 'Dagan-shadû', 'mâle', 2, 'blanc', 19, '24/04', 'ZL08', '17/10', 'ZL09', 6, 8, NULL, NULL, 'négligence', 0),
(4, 'Sîn-Tatî', 'mâle', 2, 'noir', 19, '11/02', 'ZL10', '06/04', 'ZL10', NULL, NULL, NULL, 1, NULL, 0),
(5, 'Eshtar-Addu', 'femelle', 1, 'tacheté', 20, '07/08', 'ZL02', '11/09', 'ZL10', 4, 1, NULL, NULL, NULL, 0),
(6, 'Ibal-pi-Sîn', 'femelle', 1, 'tacheté', 20, '24/09', 'ZL07', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1),
(7, 'Gabêtum', 'femelle', 1, 'jaune', 20, '05/05', 'ZL09', '06/06', 'ZL09', NULL, NULL, NULL, NULL, 'lion', 0),
(8, 'Sibku', 'mâle', 4, 'noir', 20, '07/05', 'ZL07', '01/04', 'ZL08', NULL, NULL, 1, NULL, NULL, 0),
(9, 'Anada-Libur', 'femelle', 1, 'tacheté', 20, '23/06', 'ZL08', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1),
(10, 'Hazâla', 'femelle', 1, 'noir', 20, '04/12', 'ZL06', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1),
(11, 'Puzur', 'mâle', 3, 'jaune', 21, '24/05', 'ZL07', '11/09', 'ZL10', 7, 1, NULL, NULL, NULL, 0),
(12, 'Akka-Addu', 'femelle', 1, 'tacheté', 21, '30/04', 'ZL05', '11/09', 'ZL10', 7, 1, NULL, NULL, NULL, 0),
(13, 'Sibku', 'mâle', 4, 'noir', 21, '14/06', 'ZL04', '06/09', 'ZL10', NULL, 6, NULL, NULL, NULL, 0),
(14, 'Inib-Sîna', 'femelle', 4, 'noir', 21, '12/02', 'ZL09', '06/09', 'ZL09', NULL, 6, NULL, NULL, NULL, 0),
(15, 'Aqba', 'femelle', 4, 'noir', 21, '19/05', 'ZL08', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1),
(16, 'Ahum', 'femelle', 4, 'noir', 21, '30/04', 'ZL07', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1),
(17, 'Nishum', 'mâle', 1, 'jaune', 22, '22/03', 'ZL06', '13/09', 'ZL06', NULL, NULL, NULL, 2, NULL, 0),
(18, 'Muka', 'mâle', 1, 'jaune', 22, '23/04', 'ZL06', '13/09', 'ZL06', NULL, NULL, NULL, 2, NULL, 0),
(19, 'Shûda', 'mâle', 1, 'jaune', 22, '14/05', 'ZL06', '13/09', 'ZL06', NULL, NULL, NULL, 2, NULL, 0),
(20, 'Addi-addu', 'mâle', 3, 'noir', 24, '06/01', 'ZL07', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1),
(21, 'Ishtar-pi-El', 'femelle', 3, 'noir', 24, '26/03', 'ZL06', '15/03', 'ZL08', NULL, NULL, NULL, NULL, 'négligence', 0),
(22, 'Dadda', 'femelle', 3, 'noir', 24, '18/08', 'ZL10', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1),
(23, 'Annunitum', 'femelle', 3, 'blanc', 24, '02/05', 'ZL07', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1),
(24, 'Ilu-ibni', 'femelle', 3, 'noir', 24, '14/09', 'ZL05', '31/12', 'ZL09', NULL, NULL, NULL, NULL, 'négligence', 0),
(25, 'Alat-inu', 'mâle', 1, 'jaune', 24, '23/04', 'ZL08', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1),
(26, 'Izhizzum', 'mâle', 4, 'tacheté', 23, '04/05', 'ZL07', '03/10', 'ZL07', NULL, NULL, NULL, 3, NULL, 0),
(27, 'Shubat', 'mâle', 1, 'noir', 23, '20/09', 'ZL07', '03/10', 'ZL07', NULL, NULL, NULL, 3, NULL, 0),
(28, 'Enlil-Addu', 'mâle', 2, 'blanc', 23, '30/07', 'ZL07', '03/10', 'ZL07', NULL, NULL, NULL, 3, NULL, 0),
(29, 'Tazuwâ', 'femelle', 4, 'noir', 23, '23/08', 'ZL08', '01/03', 'ZL10', NULL, NULL, NULL, NULL, 'négligence', 0),
(30, 'Tir-hazû', 'femelle', 1, 'jaune', 23, '15/09', 'ZL06', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1),
(31, 'Mâshum', 'mâle', 3, 'blanc', 25, '31/01', 'ZL05', '25/01', 'ZL08', NULL, NULL, 5, NULL, NULL, 0),
(32, '', 'femelle', 2, 'tacheté', 25, '18/08', 'ZL01', '25/01', 'ZL08', NULL, NULL, 5, NULL, NULL, 0);

--
-- Contenu de la table `parente`
--

INSERT INTO `parente` (`id_mouton_p`, `id_mouton_e`, `lien_parente`) VALUES
(1, 4, 'mère'),
(2, 1, 'mère'),
(3, 4, 'père'),
(9, 7, 'mère'),
(11, 22, 'père'),
(13, 26, 'père');

--
-- Contenu de la table `participant`
--

INSERT INTO `participant` (`id_concours`, `id_mouton`, `classement_mouton`) VALUES
(1, 1, 34),
(1, 2, 1),
(2, 5, 56),
(2, 6, 13),
(2, 9, 6),
(2, 12, 123),
(3, 8, 23),
(3, 11, 6),
(3, 13, 1);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;